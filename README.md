# Ansible role for Spamassassin installation

## Introduction

[Spamassassin](http://spamassassin.apache.org/) is an anti-spam server.

This role installs and configure the server. It also enable Pyzor
checks.

If you need to add custom rules in special tasks/roles, you can drop
bits of configuration inside `/etc/mail/spamassassin/local.cf.d/` and
call the `regenerate spamassassin configuration` handler to add it
to the configuration.

## Usage

To read parameters documentation, use this command:

```
ansible-doc -t role spamassassin
```
